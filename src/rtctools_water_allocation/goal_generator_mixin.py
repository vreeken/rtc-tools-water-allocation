from typing import Dict, List

from rtctools.optimization.timeseries import Timeseries

from .water_allocation_mixin import WaterAllocationMixin


class GoalDefinition:
    """
    A goal definition object.

    :cvar generator_type: Generator to be used to construct the goal (required).
    :cvar goal_id: Goal identifier used in the priority grouping list (required).
    :cvar goal_type: Goal type added to the optimization problem (required).
    :cvar applies_to_state_series_id: Identifies Modelica variable the goal is applied to (required)
    :cvar target_min_id: Identifies series providing the target minimum constraint (RangeGoals,
                         Minimum Goals).
    :cvar target_max_id: Identifies series providing the target maximum constraint (RangeGoals).
    :cvar target_id: Identifies series providing the target constraint (Forcing Goals).
    :cvar multiplier_fixed: Numeric multiplier value to apply to forcing series, RTC-Tools
                            convention: addition=positive, extraction=negative (required for
                            ForcingRequestGenerator).
    :cvar multiplier_attribute: Reference to attribute in branches-shape file holding element
                                dependent multiplier. Can be applied to forcings, minimum goals or
                                as weight value
    :cvar min_value: Numeric value not to be undershot by the target series
    :cvar corrective_min_value: Corrective value (numeric) to apply when the series value
                                undershoots the minimum value
    :cvar max_value: Numeric value not to be exceeded by the target series
    :cvar corrective_max_value: Corrective value (numeric) to apply when the series value exceeds
                                the maximum value
    :cvar missing_value: Numeric value representing a missing value
    :cvar corrective_missing_value: Corrective value (numeric) to apply when the series value is
                                    missing

    TODO: Reduce the number of parameters above. Should be less.
    """


class GoalGeneratorMixin(WaterAllocationMixin):
    """
    Meta class that allows you to generate goals based on the existence of certain flags or
    timeseries.
    """

    def pre(self):
        pass

    def pattern_timeseries(self) -> Dict[str, Timeseries]:
        return []

    def goal_definitions(self) -> List[GoalDefinition]:
        return []


class ConfigGoalGeneratorMixin(GoalGeneratorMixin):
    """
    Meta class that uses a certain foramt of config files to implement the methods required by
    GoalGeneratorMixin.
    """

    pass
