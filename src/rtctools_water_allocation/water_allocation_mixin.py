from typing import List

from .goals import ForcingGoal, RatioGoal
from .lookup_table import LookupTable


class WaterAllocationMixin:
    """
    Adds handling of water allocation objects in your model to your optimization
    problem.

    It performs a lookup of the relevant elements in the Modelica model,
    assuming the model is not flattened. Furthermore, it supports special
    handling of forcings, and some other goals that are part of the Water
    Allocation library as well.

    The supported Modelica elements are:
        - `Deltares.ChannelFlow.SimpleRouting.Storage.QSO`
        - `Deltares.ChannelFlow.SimpleRouting.Branches.Integrator`
        - `Deltares.ChannelFlow.SimpleRouting.Branches.Steady`
    """

    def pre(self):
        super().pre()

        raise NotImplementedError

    def forcing_goals(self) -> List[ForcingGoal]:
        """
        Behind the scenes, :py:class:`WaterAllocationMixin` will use this list
        of ForcingGoals to translate them into multiple normal goals. It will
        also take care of taking the cumulative sum, and writing out relevant
        timeseries for e.g. shortage.
        """
        return []

    def forcing_path_goals(self) -> List[ForcingGoal]:
        """
        Behind the scenes, :py:class:`WaterAllocationMixin` will use this list
        of ForcingGoals to translate them into multiple normal goals. It will
        also take care of taking the cumulative sum, and writing out relevant
        timeseries for e.g. shortage.
        """
        return []

    def ratio_goals(self) -> List[RatioGoal]:
        return []

    def ratio_path_goals(self) -> List[RatioGoal]:
        return []

    @property
    def lookup_hav(self, element: str) -> LookupTable:
        # TODO: Typing info of LookupTable also needs [float, float, float] suffix?
        raise NotImplementedError

    def post(self):
        """
        Generates a bunch of additional timeseries:

        - For volume elements with a HAV table entry, it will translate the volume into a water
          level.
        - Aggregrate and deaggrate of forcings (and shortages)
        """
        super().post()

        raise NotImplementedError
