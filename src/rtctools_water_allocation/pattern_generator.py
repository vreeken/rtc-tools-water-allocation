from datetime import datetime
from typing import Dict, List, Union

from rtctools.optimization.timeseries import Timeseries


def pattern_to_timeseries(
    pattern: List[str],
    datetimes: List[datetime],
    kind: str = "previous",
    start_datetime_index: Union[int, datetime] = 0,
) -> List[Timeseries]:
    """
    Generate :py:class:`~rtctools.optimization.timeseries.Timeseries` based
    on a source list of (datetime) patterns, and a target list of actual
    datetimes. Note that the returned Timeseries always has _seconds_ as its
    `.times` property, not datetimes.

    :param pattern: A list of strings representing the format. An example of a
        format string would be "YYYY-03-01 00:00:00".
    :param datetimes: The datetimes for which the timeseries needs to be generated.

    :param kind: The type of interpolation to be used. Supported types are
        `nearest`, `previous`, `next`, and `linear`.

    :param start_datetime_index: The (index of the) datetime to be used to
        calculate offset in seconds with, because Timeseries have times in
        seconds.
    """
    raise NotImplementedError


def csv_pattern_to_timeseries(
    file: str, pattern_orientation: str = "row", separator: str = ",", *args, **kwargs
) -> Dict[str, Timeseries]:
    """
    A convenience method to directly generate patterns from a CSV file.

    :param file: The file containing the patterns and timeseries with values.
        Assuming a "column" orientation, the header would be a list of comma-separated
        names of timeseries (with the first value the name of the index "Time" or
        "UTC"), and each row would consist of the pattern of the timeseries followed
        by a list of values (one for each timeseries).
    :param pattern_orientation: The orientation of the file; either `row` or `column`.

    All other arguments and keyword arguments are passed to `pattern_to_timeseries`.

    ..note::
        This method return a _dictionary_, mapping the name of a timeseries to its generated
        instance.
    """
    raise NotImplementedError
