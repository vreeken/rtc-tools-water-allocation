from rtctools.optimization.goal_programming_mixin import Goal


class VectorizeGoal(Goal):
    """
    Very similar to its base class, but offers vectorization capabilities.
    """

    pass


class RangeGoal(VectorizeGoal):
    """
    Other goals in this module can be very similar, but by using specialized
    types we can leverage special processing (e.g. automatic conversion), and
    also leave open the option of other more specialized processing in the
    future. Having a shared base type also helps, and allows users to define
    their own types derived from this class.
    """

    @staticmethod
    def _zeroth(v):
        raise NotImplementedError

    @staticmethod
    def _check_equal(field, *args):
        raise NotImplementedError

    @staticmethod
    def _stack_timeseries(args):
        raise NotImplementedError

    @staticmethod
    def _stack_function_range(args):
        raise NotImplementedError

    def vectorize_fields(self):
        raise NotImplementedError

    @classmethod
    def vectorize(cls, goals):
        raise NotImplementedError


class CapacityConstraintGoal(RangeGoal):
    """
    Signifies a certain range in which we want an elements (Steady or Integrator) QIn or QOut to be.
    """

    pass


class RequestRangeGoal(RangeGoal):
    """
    Goal to signify a certain desired discharge to be extracted (outflow).
    """

    pass


class LevelRangeGoal(RangeGoal):
    """
    The range in which a certain element must be. Only valid for QSO and Integrators.

    ..note::

        This goal requires WaterAllocationMixin to provide a lookup from H to V.
    """

    pass


class DischargeRangeGoal(RangeGoal):
    """
    TODO
    """

    pass


class MinimizeDischargeGoal(Goal):
    """
    Minimize a certain discharge. Typically done at the last priority.
    """

    pass


class LinearVQGoal(Goal):
    """
    Relates a discharge to a certain volume. The targets can be equal to each other, but it can also
    be a certain range to allow for some slack.

    This goal requires the user to tell it how one wants to linearize. There are generally two
    options:

    1. Linearize around a certain value
    2. Use mixed-integer programming to piecewise linearize a large part of the table.

    Both options require a certain target value or target range (for option 2.
    None and None are fine, which will lead to the full table being piecewise
    linearized).

    When using piecewise linearization, it is also important to tell how many linearizations you
    want.
    """

    pass


class LinearRefQGoal(Goal):
    """
    A more generic version of LinearVQGoal. It requires an additional lookup table argument, but is
    otherwise very similar.
    """

    pass


class ConditionalGoal(Goal):
    """
    TODO
    """

    pass


class RatioGoal(Goal):
    """
    Goal that allows for targeting a certain ratio between two variables, typically two discharges.

    To do this in a linear (convex) way, an auxiliary variable is introduced by WaterAllocationMixin
    behind the scenes.
    """

    pass


class ForcingGoal:
    """
    Goal that describes per forcing element, at what priority what discharge is desired.

    These goals cannot be passed with normal `goals()`, but instead have to be passed to
    `forcing_goals()`, such that they can be processed by  the WaterAllocationMixin.
    """

    pass
