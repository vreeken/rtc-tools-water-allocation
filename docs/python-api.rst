Python API
==========

Water Allocation
----------------

.. automodule:: rtctools_water_allocation.water_allocation_mixin
    :members: WaterAllocationMixin
    :special-members: __init__, __getattr__
    :show-inheritance:

Goal Generators
---------------

.. automodule:: rtctools_water_allocation.goal_generator_mixin
    :members: GoalDefinition, GoalGeneratorMixin, ConfigGoalGeneratorMixin
    :special-members: __init__, __getattr__
    :show-inheritance:

Goals
-----

.. automodule:: rtctools_water_allocation.goals
    :members: VectorizeGoal, RangeGoal, CapacityConstraintGoal, RequestRangeGoal, LevelRangeGoal, DischargeRangeGoal, MinimizeDischargeGoal, LinearVQGoal, LinearRefQGoal, ConditionalGoal, RatioGoal, ForcingGoal
    :special-members: __init__, __getattr__
    :show-inheritance:

Helper modules
--------------

.. automodule:: rtctools_water_allocation.pattern_generator
    :members: pattern_to_timeseries, csv_pattern_to_timeseries
    :special-members: __init__, __getattr__
    :show-inheritance:

.. automodule:: rtctools_water_allocation.lookup_table
    :members: LookupTable
    :special-members: __init__, __getattr__
    :show-inheritance:


