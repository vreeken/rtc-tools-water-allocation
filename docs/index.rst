.. RTC-Tools Water Allocation documentation master file, created by
   sphinx-quickstart on Thu Jan 17 21:21:26 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RTC-Tools Water Allocation's documentation!
======================================================

Contents:
=========

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   getting-started
   support

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   python-api

.. toctree::
   :maxdepth: 2
   :caption: Examples

   examples/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
