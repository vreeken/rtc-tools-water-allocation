###############
Getting Started
###############

This package builds on top of `RTC-Tools 2 <https://gitlab.com/deltares/rtc-tools.git>`_,
and also relies heavily on the `ChannelFlow library <https://gitlab.com/deltares/rtc-tools-channel-flow.git>`_.

Installation
============

Installation of the RTC-Tools Water Allocation library is as simple as::

    pip install rtc-tools-water-allocation

Introduction
============

RTC-Tools Water Allocation consists of a bunch of mixins, classes, and helper routines. The overall structure is as follows:

    1. :py:class:`.WaterAllocationMixin` Lowest level class, where you are
    required to specify all goals yourself. It provides additional methods for
    forcing goals and range goals, which are then automatically translated
    into the correct goal formulation.

    2. :py:class:`.GoalGeneratorMixin` Middle-level class. You can specify
    certain so-called :py:class:`.GoalDefinition` objects, which are
    translated into goals when the corresponding timeseries exist.

    3. :py:class:`.ConfigGoalGeneratorMixin` High-level class. You can specify
    the goal definitions in text files instead of Python.

    In addition to this layered approach, there are also some helper
    functions:

    1. :py:func:`.pattern_to_timeseries` Generate Timeseries objects based on
    a (repeating) datetime pattern specification.

    2. :py:func:`.csv_pattern_to_timeseries` You can specify the patterns with
    CSV files instead of Python.

Contribute
==========

You can contribute to this code through Pull Request on GitLab_. Please, make
sure that your code is coming with unit tests to ensure full coverage and
continuous integration in the API.

.. _GitLab: https://gitlab.com/deltares/rtc-tools-water-allocation/merge_requests
